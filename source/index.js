var server = require("./server"); 
var router = require("./router"); 
var requestHandlers = require("./requestHandlers"); 
 
var handle = {} 
handle["/"] = requestHandlers.parseJsonString; 
handle["/start"] = requestHandlers.parseJsonString; 
 
 
server.start(router.route, handle); 