
function parseJsonString(response, postData) { 
	 console.log("Request handler 'parseJsonString' was called.");
	 //console.log("Reiceved String is " + postData.toString());
	
	var receivedJsonObj
	var processedJsonObj = {};
	var element = [];
	
	// add element if the value exist
	function addElement(iArray, iKey, iVal) {
		if(typeof iVal === "undefined") {
			delete iArray[iKey];
		}
		else {
			iArray[iKey] = iVal;
		}
	}
	
	try{
		receivedJsonObj = JSON.parse(postData);
		
	  }catch(e){
        console.log(e); //error in the above string(in this case,yes)!
		
		var errResponse = {
			"error": "Could not decode request: JSON parsing failed"
		}
		
		response.writeHead(400, {"Content-Type": "application/json"}); 
		response.write(JSON.stringify(errResponse)); 
		response.end(); 
		return;
    }
		
	// parse the needed key and value
	var drm, episodeCount;	
	for (var i in receivedJsonObj.payload){
		drm = receivedJsonObj.payload[i].drm;
		if ((typeof(drm) != "undefined") && (drm == true)){
			episodeCount = receivedJsonObj.payload[i].episodeCount;
			if ((typeof(episodeCount) != "undefined") && (episodeCount > 0)){
				var array = {};
				addElement(array, "image", receivedJsonObj.payload[i].image.showImage);
				addElement(array, "slug", receivedJsonObj.payload[i].slug);
				addElement(array, "title", receivedJsonObj.payload[i].title);
				element.push(array);
			}
			
		}
	}
	
	processedJsonObj.response = element;
	
	console.log("Processed JSON data is " + JSON.stringify(processedJsonObj));
		 
	response.writeHead(200, {"Content-Type": "application/json"}); 
	response.write(JSON.stringify(processedJsonObj)); 
	response.end(); 
	

} 

exports.parseJsonString = parseJsonString; 
