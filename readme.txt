1. file structure
/source (mi9 coding challenge source code)
	index.js:  			the main js file with enterance function
	server.js:			start server and set up callbacks
	router.js:			router, as the name indicates
	requestHandlers.js	request handle
	
/test
	tester.js			tests

2. How to run this application
	a. set up the node.js environment
	b. cd /source
	c. node index.js
	done!easy
	
3. How to test this application
	a. deploy the application on a public host (like AWS EC2)
	b. get the host's ip or domain
	c. open "tester.js" with file editor. Find the "options.host" field and replace its' value with the host's IP or domain
	d. run the test locally 
		cd /test ; node tester.js		
	e. change the "userString" for further tests.
	done!
	